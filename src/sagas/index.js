import { takeEvery } from 'redux-saga/effects'

import { fetchData } from '../actions/items'
import { FETCHED_DATA } from '../actions/items'

export default function* watchFetchData() {
    yield takeEvery(FETCHED_DATA, fetchData);
}
