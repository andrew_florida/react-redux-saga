import * as actions from '../actions/items'

export const itemsHasError = (state = false, action) => {
    switch (action.type) {
        case actions.ITEMS_HAS_ERROR:
            return action.itemsHasError;

        default:
            return state;
    }
}

export const itemsIsLoading = (state = false, action) => {
    switch (action.type) {
        case actions.ITEMS_IS_LOADING:
            return action.itemsIsLoading;

        default:
            return state;
    }
}

export const items = (items = [], action) => {
    switch (action.type) {
        case actions.ITEMS_FETCH_DATA_SUCCESS:
            return action.items;

        case actions.ADD_TODO:
            return [
                {
                    id: action.id,
                    title: action.title,
                    edit: false
                },
                ...items
            ]

        case actions.EDIT_TODO:
            return items.map(item => (item.id === action.id) ? { ...item, edit: !item.edit } : item)

        case actions.CHANGE_TODO:
            return items.map(item => (item.id === action.id) ? { ...item, title: action.title } : item)

        case actions.SHOUTOUT_DELETED:
            return items ? items.filter((item) => item.id !== action.id) : []

        default:
            return items
    }
}