import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import './Todo.css'

const Todo = ({ onDelete, onEdit, onChange, edit, title}) => (
    <li className="list">

        <span style={{ display: edit ? 'none' : 'inline-block' }}>
            {title}
        </span>
        <input
            style={{display: edit ? 'inline-block' : 'none'}}
            onChange={onChange}
            type="text"
            defaultValue={title}
            className="list__input">
        </input>
        <button onClick={onEdit}>Edit</button>
        <button onClick={onDelete}>Delete</button>
    </li>
)

Todo.propTypes = {
    title: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
}

export default connect()(Todo)
