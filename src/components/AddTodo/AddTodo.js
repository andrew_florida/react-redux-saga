import React, { Component } from 'react'
import { connect } from 'react-redux'

import { addTodo } from '../../actions/items'
import './AddTodo.css';

class AddTodo extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
    }

    handleChange = (event)=> {
        this.setState({ value: event.target.value });
    }

    handleSubmit = (event) =>{
        event.preventDefault();
        if (this.state.value === ''){
            return
        }
        this.props.dispatch(addTodo(this.state.value));
        this.setState({ value: '' });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text"
                    value={this.state.value}
                    onChange={this.handleChange}
                    placeholder="Create task" />

                <button type="submit">Add</button>
            </form>
        );
    }
}

export default connect()(AddTodo)