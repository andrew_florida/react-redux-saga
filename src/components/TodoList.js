import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { itemsFetchData, onEdit, onChange, onDelete } from '../actions/items';
import AddTodo from './AddTodo';
import Todo from './Todo'

const mapStateToProps = (state) => {
    return {
        items: state.items,
        itemsHasError: state.itemsHasError,
        itemsIsLoading: state.itemsIsLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({itemsFetchData, onChange, onDelete, onEdit }, dispatch)
    };
};

class TodoList extends Component {
    componentDidMount() {
        this.props.itemsFetchData();
    }
    
    render() {
        if (this.props.itemsHasError) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        if (this.props.itemsIsLoading) {
            return <p>Loading…</p>;
        }

        return (
            <div className="todos" >
                <AddTodo />
                <ul>
                    {this.props.items.map((item) => (
                        <Todo
                            title={item.title}
                            key={item.id}
                            id={item.id}
                            edit={item.edit}
                            onDelete={() => this.props.onDelete(item.id)}
                            onChange={(e) => this.props.onChange(e.target.value, item.id)}
                            onEdit={() => this.props.onEdit(item.id)}
                        />
                    ))}
                </ul>
            </div>
        );
    }

}

TodoList.propTypes = {
    itemsFetchData: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    itemsHasError: PropTypes.bool.isRequired,
    itemsIsLoading: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);

