import React from 'react';
import { render } from 'react-dom';
import { Provider} from 'react-redux';

import TodoList from './components/TodoList';
import {store} from './store/configureStore';

render(
    <Provider store={store}>
        <TodoList />
    </Provider>,
    document.getElementById('root'));