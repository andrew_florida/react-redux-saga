import { put, call } from 'redux-saga/effects'

export const ITEMS_IS_LOADING = 'ITEMS_IS_LOADING'
export const ITEMS_FETCH_DATA_SUCCESS = 'ITEMS_FETCH_DATA_SUCCESS'
export const ITEMS_HAS_ERROR = 'ITEMS_HAS_ERROR'
export const FETCHED_DATA = 'FETCHED_DATA'
export const ADD_TODO = 'ADD_TODO'
export const EDIT_TODO = 'EDIT_TODO'
export const SHOUTOUT_DELETED = 'SHOUTOUT_DELETED'
export const CHANGE_TODO = 'CHANGE_TODO'

let api = (url) => fetch(url).then(res => res.json())
let nextTodoId = 201

export function* fetchData(action) {
    try {
        yield put(itemsIsLoading(true));
        const items = yield call(api, ' https://jsonplaceholder.typicode.com/todos/');
        if (items && items.length > 0) {
            yield put(itemsIsLoading(false));
            yield put(itemsFetchDataSuccess(items));
        }
    }
    catch(e){
        yield put(itemsIsLoading(false));
        yield put(itemsHasError(true));
        console.log(e)
    }
        
        
}

export const itemsFetchData = () => ({
    type: FETCHED_DATA
});

export const itemsHasError = bool => ({
    type: ITEMS_HAS_ERROR,
    itemsHasError: bool
})

export const itemsIsLoading = bool => ({
    type: ITEMS_IS_LOADING,
    itemsIsLoading: bool
})

export const itemsFetchDataSuccess = items => ({
    type: ITEMS_FETCH_DATA_SUCCESS,
    items
})

export const addTodo = title => ({
    type: ADD_TODO,
    id: nextTodoId++,
    title
})

export const onEdit = id => ({
    type: EDIT_TODO,
    id
})

export const onDelete = id => ({
    type: SHOUTOUT_DELETED,
    id
})

export const onChange = (e, id) => ({
    type: CHANGE_TODO,
    title: e,
    id
})